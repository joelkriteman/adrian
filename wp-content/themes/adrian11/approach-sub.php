<?php
/*
Template Name: Approach-sub
*/
?>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
      'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
 <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<meta name="Description" content="" />
	<meta name="keywords" content="" />
	<meta name="url" content="" />
	<meta name="rating" content="Safe For Kids" />
	<meta name="revisit-after" content="30 days" />
	<meta name="robots" content="all" />
	<meta name="copyright" content="" />
	<title>Adrian Goodall Associates</title>
	<?php include "includes.php";?>
</head>
		<body id="<?php echo $post->post_name; ?>">
			<!-- Begin Wrapper -->
				<div id="wrapper">				
					<?php if (have_posts()) : ?>

											<?php while (have_posts()) : the_post(); ?>
				
					<div id="topleft">
					<?php $image = get_field('headerleft');
                    $size = 'headerleft'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) { echo wp_get_attachment_image( $image, $size ); } ?>
          </div>
					<img src="<?php bloginfo('template_directory'); ?>/images/logo-2021.jpg" id="logo" alt="" />


					<ul class="social">
										<li><a href="http://www.linkedin.com/in/adriangoodall" rel="external"><img src="<?php bloginfo('template_directory'); ?>/images/linkedin.png" alt="linkedin" /></a></li>
										<li><a href="mailto:adrian@adriangoodall.co.uk"><img src="<?php bloginfo('template_directory'); ?>/images/email.png" alt="email" /></a></li>
								</ul>
				
					<!-- begin header -->
						<div id="header">
						
							<?php include "mainnav.php";?>
							
							<div id="breadcrumb">
								<ul>
									<li><a href="<?php bloginfo('home'); ?>">home</a></li>
									<li>></li>
									<li><a href="<?php bloginfo('home'); ?>/leaders">leaders</a></li>
									<li>></li>
									<li class="current"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								</ul>
							</div>
							
						</div>
					<!-- end header -->
					
					<!-- Begin content -->
					<div id="content">

											
				
											<div id="centrecolumn">
														<div class="bodyimages"><?php meta('bodyimages'); ?></div>
														<?php the_content(); ?>
														
														<?php endwhile; ?>


														<?php else : ?>
													
															<h2 class="center">Not Found</h2>
															<p class="center">Sorry, but you are looking for something that isn't here.</p>
															<?php get_search_form(); ?>
													
														<?php endif; ?>
														
														<div class="back"><a href="<?php bloginfo('home'); ?>/leaders">back to leaders</a></div>
						
											</div>
						
											<?php include "leftcolumn.php";?>
						
					</div>
					<!-- End content -->
												
					
				</div>
			   <!-- End Wrapper -->
			   
			   
		</body>
		

</html>