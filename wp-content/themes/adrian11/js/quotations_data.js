var aQuotations =
[
 { 'quote': '&quot;Adrian has been that rare thing, a coach of obvious, immediate and long-term value. Challenging, he has a real knack of making you work through the logic and reasons behind behaviour. Extremely likeable, I would unhesitatingly recommend him both to individuals and organisations.&quot;<br />- Director, International Communications Agency' },
 { 'quote': '&quot;I really appreciated Adrian&#8217;s coaching. The sessions were very, very helpful and productive.&quot;<br />- UK CEO, Global Corporate' },
 { 'quote': '&quot;I found Adrian&#8217;s coaching really useful and helpful. I now have much more informed thoughts on how I do what I do, what I&#8217;ve got that I can use. Do I limit myself? Not anymore. A huge step up from 6 months ago.&quot;<br />- Senior Finance executive, Global Corporate' }, 
 { 'quote': '&quot;Clearly getting value from the sessions and having an impact in the firm&quot;<br />- Partner, Law Firm' },
 { 'quote': '&quot;Gave me the tools to be able to get up in front of a room full of investors and speak with confidence - something I never thought I would be able to do.&quot;<br />- CEO, Engineering firm' },
 { 'quote': '&quot;Adrian did a fantastic job yesterday. Given that we went into the day with high expectations, we walked away feeling we made real progress and achieved what we set out to do. It was hard work, but Adrian did an excellent job in ensuring that we kept to the schedule and the objectives. This I feel has helped to improve the team&#8217;s motivation significantly which should set us up for all the work ahead of us!&quot;<br />- Executive &amp; Team leader, Financial Services' },
 { 'quote': '&quot;It&#8217;s been great working with you - I really appreciate your warmth, encouragement and skill as a coach - I found your skill in working with my metaphors very helpful and gained some valuable insights from this. Thank you&quot;<br />- Human Resources executive' },
  { 'quote': '&quot;As an entrepreneur, the biggest challenges I face are mental blocks which can be both costly and frustrating. I have found Adrian&#8217;s assistance in this area invaluable in enabling me to work effectively through these periods - and to produce some of my best work.&quot;<br />- Software Entrepreneur' },
 { 'quote': '&quot;The changes already are amazing- I have so much more clarity about the rest of this year.&quot;<br />- Manager, Youth Charity' }
];

var nFadeSeconds = 1;
var nDisplaySeconds = 10;