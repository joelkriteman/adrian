function Quotations() {

  var self = this;
  var nQuotations;
  
  this.initialise = function() {
    this.nQuotations = aQuotations.length - 1;
    this.shuffle();
    this.fade();
  };
  
  this.shuffle = function() {
    aQuotations = $.shuffle(aQuotations);
  };
  
  this.fade = function() {
    $('#quotation_'+self.nQuotations).animate({'opacity':0}, nFadeSeconds * 2000,  function() { $(this).remove() });
    self.nQuotations = (self.nQuotations += 1) >= aQuotations.length  ? 0 : self.nQuotations;
    $('#quotations').append(self.quotationHTML());
    $('#'+self.quotationId()).css('opacity',0);
    $('#'+self.quotationId()).animate({'opacity':1}, nFadeSeconds * 2000,  self.startTimer);
  };
  
  this.startTimer = function() {
    setTimeout(self.fade, nDisplaySeconds * 2000);
  };
  
  this.quotationId = function() {
    return 'quotation_'+self.nQuotations;
  };
  
  this.quotationHTML = function() {
    return '<div id="'+self.quotationId()+'"><span></span>'+aQuotations[self.nQuotations].quote+'<span></span></div>';
  };
};


(function($){
  $.shuffle = function(arr) {
    for(
      var j, x, i = arr.length; i;
      j = parseInt(Math.random() * i),
      x = arr[--i], arr[i] = arr[j], arr[j] = x
    );
    return arr;
  }
})(jQuery);


$(document).ready(function(){
  oQuotations = new Quotations();
  oQuotations.initialise();
});