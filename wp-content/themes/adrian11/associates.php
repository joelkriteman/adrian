<?php
/*
Template Name: Associates
*/
?>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
      'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
 <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<meta name="Description" content="" />
	<meta name="keywords" content="" />
	<meta name="url" content="" />
	<meta name="rating" content="Safe For Kids" />
	<meta name="revisit-after" content="30 days" />
	<meta name="robots" content="all" />
	<meta name="copyright" content="" />
	<title>Adrian Goodall Associates</title>
	<?php include "includes.php";?>
</head>
		<body id="<?php echo $post->post_name; ?>">
			<!-- Begin Wrapper -->
				<div id="wrapper">				
					<?php if (have_posts()) : ?>

											<?php while (have_posts()) : the_post(); ?>
				
					<div id="topleft">
					<?php $image = get_field('headerleft');
                    $size = 'headerleft'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) { echo wp_get_attachment_image( $image, $size ); } ?>
          </div>
					<img src="<?php bloginfo('template_directory'); ?>/images/logo-2021.jpg" id="logo" alt="" />

					<ul class="social">
										<li><a href="http://www.linkedin.com/in/adriangoodall" rel="external"><img src="<?php bloginfo('template_directory'); ?>/images/linkedin.png" alt="linkedin" /></a></li>
										<li><a href="mailto:adrian@adriangoodall.co.uk"><img src="<?php bloginfo('template_directory'); ?>/images/email.png" alt="email" /></a></li>
								</ul>
				
					<!-- begin header -->
						<div id="header">
						
							<?php include "mainnav.php";?>
							
							<div id="breadcrumb">
								<ul>
									<li><a href="http://www.adriangoodall.co.uk">home</a></li>
									<li>></li>
									<li class="current"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								</ul>
							</div>
							
						</div>
					<!-- end header -->
					
					<!-- Begin content -->
					<div id="content">

											
				
									<div id="centrecolumn">
									
											<p>Adrian Goodall Associates' team provides a range of senior level commercial and organisational experience, focus on leadership, change and excellence, and share values and an approach to coaching with training of the highest quality.</p>
											
											<p>Please click on the images below for more information.</p>
											
											<div class="clear"></div>
											
											<ul class="assocblock">
												
												<li><a href="http://www.adriangoodall.co.uk/adrian"><img src="<?php bloginfo('template_directory'); ?>/images/Adrianforweb.jpg" alt="" />Adrian Goodall</a></li>
												<li><a href="http://www.adriangoodall.co.uk/kevin"><img src="<?php bloginfo('template_directory'); ?>/images/Kevinforweb.jpg" alt="" />Kevin Caulfield</a></li>
												<li><a href="http://www.adriangoodall.co.uk/deborah"><img src="<?php bloginfo('template_directory'); ?>/images/deborahforweb.jpg" alt="" />Deborah Goodall</a></li>
												<li class="assocright"><a href="http://www.adriangoodall.co.uk/andy"><img src="<?php bloginfo('template_directory'); ?>/images/Andyforweb.jpg" alt="" />Andy King</a></li>
												
												<li><a href="http://www.adriangoodall.co.uk/rachael"><img src="<?php bloginfo('template_directory'); ?>/images/Rachaelforweb.jpg" alt="" />Rachael Ross</a></li>
												<li><a href="http://www.adriangoodall.co.uk/derryn"><img src="<?php bloginfo('template_directory'); ?>/images/Derynforweb.jpg" alt="" />Deryn Holland</a></li>
												<li><a href="http://www.adriangoodall.co.uk/kate"><img src="<?php bloginfo('template_directory'); ?>/images/Kateforweb.jpg" alt="" />Kate Renshaw</a></li>
												<li class="assocright"><a href="http://www.adriangoodall.co.uk/lorenza"><img src="<?php bloginfo('template_directory'); ?>/images/Lorenzaforweb.jpg" alt="" />Lorenza Clifford</a></li>
											</ul>
											
											<div class="clear"></div>
											
											<?php the_content(); ?>
														
														<?php endwhile; ?>


														<?php else : ?>
													
															
													
														<?php endif; ?>			
														
											

						
									</div>
						
											<?php include "leftcolumn.php";?>
						
					</div>
					<!-- End content -->
												
					
				</div>
			   <!-- End Wrapper -->
			   
			   
		</body>
		

</html>