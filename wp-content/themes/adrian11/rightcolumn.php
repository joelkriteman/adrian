						<div id="rightcolumn">
							<div id="rightnav">
								<h3>categories</h3>
								<ul class="navigation-right">
										<?php
$args=array(
  'orderby' => 'name',
  'order' => 'ASC'
  );
$categories=get_categories($args);
  foreach($categories as $category) { 
    echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a></li> ';
  } 
?>
								</ul>
								
								<h3>blog posts</h3>
								<ul class="navigation-right">
										<?php
									 global $post;
									 $myposts = get_posts('numberposts=5');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>								
									 <?php endforeach; ?>
								</ul>
								
								
								<div class="clear"></div>
							</div>
							
							<div id="carciofino">
								<a href="http://www.carciofino.com" rel="external">website by carciofino</a>
							</div>
						</div>