<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/externallinks.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/quotations_data.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/quotations.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?rnd=123" />
<script type="text/javascript">

    var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24743259-1']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	<?php wp_head(); ?>