							<div id="leftcolumn">
							
								<h2>&quot;<?php the_field('headerquote'); ?>&quot;</h2>
									<h3>blog posts</h3>
								<ul class="navigation-left">
										<?php
									 global $post;
									 $myposts = get_posts('numberposts=5');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>
											<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>								
									 <?php endforeach; ?>
									 <li class="allposts"><a href="<?php bloginfo('home'); ?>/blog">all blog posts</a></li>
								</ul>
								
								
								<div id="quotations">
									 </div>			
							</div>
							
							<div class="clear"></div>